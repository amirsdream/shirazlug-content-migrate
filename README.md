# Content Converter

A simple script to download and convert the content of the old website of [Shirazlug](https://shirazlug.ir) to hugo format.

## How to run
clone the repository,
> git clone https://gitlab.com/shirazlug/shirazlug-content-migrate.git

go inside the project directory 

> cd shirazlug-content-migrate

and simply run convert.py

> ./convert.py

( if you had problems with executing it, try changing permissions)

> chmod +x convert.py 

## TODO list

- [x] add readme
- [x] update readme
- [ ] converting posts
- [ ] adding other sections to the Convertor
